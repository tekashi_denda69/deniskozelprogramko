import java.util.Scanner;

public class ukol1 {
    public static void main(String[] args) {
        int cislo = 0;
        int pocetKladnych = 0;
        int pocetNulovych = 0;
        int pocetZapornych = 0;
        int soucetZapornych = 0;
        Scanner skenr = new Scanner(System.in, "UTF-8");

        System.out.print("Zadej číslo: ");
        cislo = skenr.nextInt();
        while(cislo != -999){
            if(cislo > 0){
                pocetKladnych = pocetKladnych + 1; //také je možné pocetKladnych++
            }
            if(cislo == 0){
                pocetNulovych = pocetNulovych + 1; //také je možné pocetNulovych++
            }
            if(cislo < 0){
                pocetZapornych = pocetZapornych + 1; //také je možné pocetZapornych++
                soucetZapornych = soucetZapornych + cislo;
            }
            System.out.print("Zadej číslo: ");
            cislo = skenr.nextInt();
        }
        System.out.println("Počet kladných čísel: " + pocetKladnych);
        System.out.println("Počet nulových čísel: " + pocetNulovych);
        if(pocetZapornych > 0){
            System.out.println("Počet záporných čísel: " + pocetZapornych);
            System.out.println("Součet záporných čísel: " + soucetZapornych);
        }

    }
}