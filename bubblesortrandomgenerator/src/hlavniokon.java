import java.util.Arrays;

public class hlavniokon {

    public static void main(String[] args) {
        // Generátor čísel
        int[] list = new int[10];
        for(int i = 0; i < list.length; i++) {
        list[i] = (int)(Math.random()*100 + 1);
    }
        // Vyprintuje nám hodnoty
        System.out.println("Generované neroztříděná čísla: " + Arrays.toString(list));
        bubbleSort(list);
        System.out.println("Bubble Sort roztříděná čísla:" + Arrays.toString(list));

}

    // Metoda, kterou jste nám ukazovala v hodině
    public static void bubbleSort(int[] list) {
        int j = list.length - 2, temp;
        boolean swapped = true;
        while (swapped) {
            swapped = false;
            for (int i = 0; i <= j; i++) {
                if (list[i] > list[i + 1]) {
                    temp = list[i];
                    list[i] = list[i + 1];
                    list[i + 1] = temp;
                    swapped = true;

                }
            }
        }
    }
}