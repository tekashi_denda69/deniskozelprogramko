
import javax.swing.*;
import java.awt.*;

public class HlavniOkno extends JFrame {
    public void nastavKomponenty(){
        setDefaultCloseOperation(WindowConstants.DISPOSE_ON_CLOSE);
        setTitle("Čas strávený cestováním do školy");
        setSize(800,450);
        setLocationRelativeTo(null);


        ImageIcon background_image = new ImageIcon("1.jpg");
        JLabel background = new JLabel("", background_image, JLabel.CENTER);
        Image img = background_image.getImage();
        Image temp_img = img.getScaledInstance(605,350 , Image.SCALE_SMOOTH);
        background.setBounds(0,0,605,350);
        background_image = new ImageIcon(temp_img);

       Container contentPane = getContentPane();
       background.setLayout(null);
       background.setVisible(true);



        add(background);
        JTextField txt1Cesta = new JTextField();
        JTextField txtCestazpet = new JTextField();

        JButton btnTlacitko = new JButton();
        btnTlacitko.setBounds(30, 120, 540, 30);
        btnTlacitko.setText("VYPOČTI");

        JLabel labNadpis = new JLabel();
        labNadpis.setBounds(5, 1, 605, 30);
        labNadpis.setText("Čas strávený cestováním do školy");
        labNadpis.setFont(new Font("Monospaced", Font.ITALIC, 25));
        JLabel lab1Cesta = new JLabel();
        lab1Cesta.setBounds(5, 30, 605, 30);
        lab1Cesta.setText("Kolik minut zabere 1 cesta?");
        txt1Cesta.setBounds(225,30,50,30);
        lab1Cesta.setFont(new Font("TimesRoman", Font.BOLD, 13));
        JLabel labCestazpet = new JLabel();
        labCestazpet.setBounds(5, 60, 605, 30);
        labCestazpet.setText("Jak dlouho se zdržíš při cestě zpět?");
        labCestazpet.setFont(new Font("TimesRoman", Font.BOLD, 13));
        txtCestazpet.setBounds(225,60,50,30);
        JLabel labInfo = new JLabel();
        labInfo.setBounds(5, 170, 1000, 60);
        labInfo.setText("Cesta do školy a zpět trvá i se zdržením ČÍSLOVÝPOČTU, za pololetí to je celkem.");
        labInfo.setFont(new Font("TimesRoman", Font.BOLD, 13));
        JLabel labInfo2 = new JLabel();
        labInfo2.setBounds(5, 185,1000, 60 );
        labInfo2.setText("Přičtení 1 knihy přibližně 12,3 hodin jde za pololetí při cestování přečíst POČET knih.");
        labInfo2.setFont(new Font("TimesRoman", Font.BOLD, 13));


        background.add(txt1Cesta);
        background.add(txtCestazpet);
        background.add(labCestazpet);
        background.add(lab1Cesta);
        background.add(labNadpis);
        background.add(btnTlacitko);
        background.add(labInfo);
        background.add(labInfo2);
    }
}


