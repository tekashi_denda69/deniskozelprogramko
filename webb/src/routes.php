<?php

use \Psr\Http\Message\ServerRequestInterface as Request;
use \Psr\Http\Message\ResponseInterface as Response;

$app->get('/', function (Request $request, Response $response, $args){
    return $this->view->render($response, 'index.latte');
})->setName('index');

$app->get('/nadpis1', function (Request $request, Response $response, $args){
    return $this->view->render($response, 'nadpis1.latte');
})->setName('nadpis1');

$app->get('/web/templates/nadpis2.latte', function (Request $request, Response $response, $args){
    return $this->view->render($response, 'nadpis2.latte');
})->setName('/web/templates/nadpis2.latte');

$app->get('/nadpis3', function (Request $request, Response $response, $args){
    return $this->view->render($response, 'nadpis3.latte');
})->setName('nadpis3');