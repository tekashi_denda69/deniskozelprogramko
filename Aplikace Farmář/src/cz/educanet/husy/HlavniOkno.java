package cz.educanet.husy;

import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import javax.imageio.ImageIO;
import javax.imageio.stream.FileImageInputStream;
import javax.swing.*;

public class HlavniOkno extends JFrame {

    // JFormDesigner - Variables declaration - DO NOT MODIFY  //GEN-BEGIN:variables
    // Generated using JFormDesigner Evaluation license - unknown
    // JFormDesigner - End of variables declaration  //GEN-END:variables

    public static void main(String[] args) {
        JFrame frame = new JFrame();
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        frame.setPreferredSize(new Dimension(605, 350));
        frame.getContentPane().setBackground(Color.BLUE);
        frame.pack();
        frame.setVisible(true);

    }



    Integer pocetHus;
    Integer pocetKraliku;
    Integer pocetZvirat;
    Integer pocetNohou;

    JTextField txtPocetHus = new JTextField();
    JTextField txtPocetKraliku = new JTextField();
    JLabel labPocetZvirat = new JLabel();
    JLabel labPocetNohou = new JLabel();

    public void initComponents() {
        // JFormDesigner - Component initialization - DO NOT MODIFY  //GEN-BEGIN:initComponents
        // Generated using JFormDesigner Evaluation license - unknown

        //======== this ========
        setDefaultCloseOperation(WindowConstants.DISPOSE_ON_CLOSE);
        setTitle("Farmář");
        Container contentPane = getContentPane();
        contentPane.setLayout(null);

        {
            // compute preferred size
            Dimension preferredSize = new Dimension();
            for(int i = 0; i < contentPane.getComponentCount(); i++) {
                Rectangle bounds = contentPane.getComponent(i).getBounds();
                preferredSize.width = Math.max(bounds.x + bounds.width, preferredSize.width);
                preferredSize.height = Math.max(bounds.y + bounds.height, preferredSize.height);
            }
            Insets insets = contentPane.getInsets();
            preferredSize.width += insets.right;
            preferredSize.height += insets.bottom;
            contentPane.setMinimumSize(preferredSize);
            contentPane.setPreferredSize(preferredSize);
        }
        setSize(605, 350);
        setLocationRelativeTo(null);



        // JFormDesigner - End of component initialization  //GEN-END:initComponents


        JLabel labNadpis = new JLabel();
        JLabel labJmeno = new JLabel();
        JLabel labPocetHus = new JLabel();
        JLabel labPocetKraliku = new JLabel();
        JButton btnTlacitko = new JButton();
        ImageIcon background_image = new ImageIcon("cestadoskoly\\src\\1.jpg");
        JLabel background = new JLabel("", background_image, JLabel.CENTER);
        Image img = background_image.getImage();
        Image temp_img = img.getScaledInstance(605,350 , Image.SCALE_SMOOTH);
        background.setBounds(0,0,605,350);
        background_image = new ImageIcon(temp_img);

        add(background);
        background.setVisible(true);
        background.add(labNadpis);
        background.add(labJmeno);
        background.add(labPocetHus);
        background.add(labPocetKraliku);
        background.add(labPocetZvirat);
        background.add(labPocetNohou);
        background.add(txtPocetHus);
        background.add(txtPocetKraliku);
        background.add(btnTlacitko);


        labNadpis.setBounds(275, 80, 540, 30);
        labNadpis.setText("Aplikace Farmář");
        labNadpis.setFont(new Font("TimesRoman", Font.BOLD, 32));
        labNadpis.setForeground(Color.white);

        labJmeno.setBounds(275, 100, 540, 30);
        labJmeno.setText("Vytvořil Denis Kozel 2.A EDUCAnet Brno");
        labJmeno.setForeground(Color.white);

        labPocetHus.setBounds(30, 40, 540, 30);
        labPocetHus.setText("Počet hus");
        txtPocetHus.setBounds(150, 40, 80, 30);
        labPocetHus.setForeground(Color.white);


        labPocetKraliku.setBounds(30, 80, 540, 30);
        labPocetKraliku.setText("Počet králíků");
        txtPocetKraliku.setBounds(150, 80, 80, 30);
        labPocetKraliku.setForeground(Color.white);


        labPocetZvirat.setBounds(30, 120, 540, 30);
        labPocetZvirat.setText("Počet zvířat");


        labPocetNohou.setBounds(30, 160, 540, 30);
        labPocetNohou.setText("Počet nohou");

        labPocetNohou.setForeground(Color.white);
        labPocetZvirat.setForeground(Color.white);


        btnTlacitko.addActionListener(e -> poKliknuti(e));
        btnTlacitko.setBounds(30, 240, 540, 30);
        btnTlacitko.setText("Spočítat");

    }

    private void poKliknuti(ActionEvent e) {
        pocetHus = Integer.valueOf(txtPocetHus.getText());
        pocetKraliku = Integer.valueOf(txtPocetKraliku.getText());

        pocetNohou = pocetHus * 2 + pocetKraliku * 4;
        pocetZvirat = pocetHus + pocetKraliku;

        labPocetZvirat.setText("Počet zvířat celkem: " + pocetZvirat);
        labPocetNohou.setText("Počet nohou celkem: " + pocetNohou);




    }
}
