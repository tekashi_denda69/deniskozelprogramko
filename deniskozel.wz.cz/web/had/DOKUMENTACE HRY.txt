Projekt: Snake Game
Vypracoval: Denis Kozel 2.A
Zdroj: CodeWithKris (odkaz v html)
Doba tvoření: 28.4 - 2.5 (3.5 poslední kontrola a odevzdání)

Postup práce: 	

Přemýšlel jsem jakou hru bych mohl udělat a asi bych nedokázal během pár dni naprogramovat nějakou nadupanou hru, ale takovou klasiku jako je SNAKE nebo Tetris bych
pomocí youtube mohl zvládnout. Našel jsem si video kde vysvětluje krok za krokem proč co do programu píše a snažil jsem se ho pochopit.
Sice tam využíval možnosti, které jsme se dosud neučili (například const proměnou), ale s videem a následujícím hraním s kodem jsem pochopil co a jak.

Začal jsem tedy tím, že jsem se z videa naučil, jak vlastně co funguje a proč. Měl jsem tedy soubory:

 snake.js, kde byl kod o tom jak má had vypadat, pohybovat se a fungovat.
 fruit.js, vlastně to ovoce co had sbírá, vtom je jak vypadá ovoce a jak se chová po tom co se ho snake dotkne.
 draw.js, reaguje na canvas což je vlastně to pole kde se hra zobrazuje. Je na to napojeno snake.js, fruit.js a vlastně je to soubor, který dává celou hru dohromady, vykresluje.

Měl jsem tři soubory v javě, ale to mi nestačilo. Řekl jsem si, že se to bude lépe hrát v prohlížeči. Udělal jsem tedy html kam jsem nalinkoval javascripty, udělal hrací pole přes canvas.
A měl jsem okno, v levém horním rohu, kde se dál hrát had.

To mi však nestačilo, tak jsem si dal záležet na tom aby hrací okno bylo vycentrováno, udělal jsem políčko Score, kde se ze začátku objeví nula a po sbírání dalších fruitů se číslo zvětšuje.
Přebarvil jsem hrací pole, hada i ovoce, přebarvil jsem html stránku spolu a komponenty také, přidal tomu trošku hezčí design.

Na hru jsem se díval od 28.4 do 2.5 každý den aspon půl hodiny až hodinu, podle toho jaké jsem měl nápady na úpravy. 

Instrukce: 
Pro spuštění hry otevřete html soubor.


