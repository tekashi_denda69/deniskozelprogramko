<?php
mb_internal_encoding("UTF-8");

$hlaska = '';
if ($_POST) // V poli _POST něco je, odeslal se formulář
{
    if (isset($_POST['jmeno']) && $_POST['jmeno'] &&
        isset($_POST['email']) && $_POST['email'] &&
        isset($_POST['zprava']) && $_POST['zprava'] &&
        isset($_POST['rok']) && $_POST['rok'] == date('Y'))
    {
        $hlavicka = 'From:' . $_POST['email'];
        $hlavicka .= "\nMIME-Version: 1.0\n";
        $hlavicka .= "Content-Type: text/html; charset=\"utf-8\"\n";
        $adresa = 'denis.kozel@educanet.cz';
        $predmet = 'Nová zpráva z mailformu';
        $uspech = mb_send_mail($adresa, $predmet, $_POST['zprava'], $hlavicka);
        if ($uspech)
        {
            $hlaska = 'E-mail se podařilo v pořádku odeslat.';
        }
        else
            $hlaska = 'E-mail se bohužel nepodařilo odeslat. Zkontrolujte zadané údaje.';
    }
    else
        $hlaska = 'Formulář jste nevyplnili správně. Prosím oprave chyby';
}

?>


<!DOCTYPE html>
<html lang="cs">
<head>
    <meta charset="utf-8">
    <title>Kontaktní formulář</title>
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css" integrity="sha384-Vkoo8x4CGsO3+Hhxv8T/Q5PaXtkKtu6ug5TOeNV6gBiFeWPGFN9MuhOf23Q9Ifjh" crossorigin="anonymous">
    <link rel="stylesheet" href="/public/css/styles.css">

    <meta name="viewport" content="width=device-width, initial-scale=1">
</head>
<body>
<hr>
<h1>Projekt - Kozel Denis</h1>

<li><a href="/public">Hlavni stranka</a></li>

<a href="https://brno.educanet.cz/cs/">Student EDUCANET BRNO</a>
<hr>


<h1>Kontakt</h1>
<p>Denis Kozel<br>
    E-mail: denis tecka kozel @educanet tecka com
</p>

<h2>Kontaktní formulář</h2>
<p>Chcete-li mě kontaktovat přímo z webu, využijte následující formulář.</p>

<?php
if ($hlaska)
    echo('<p>' . $hlaska . '</p>');
?>

<form method="POST">
    <table>
        <tr>
            <td>Vaše jméno</td>
            <td><input name="jmeno" type="text" /></td>
        </tr>
        <tr>
            <td>Váš e-mail</td>
            <td><input name="email" type="email" /></td>
        </tr>
        <tr>
            <td>Jaký je nyní rok?</td>
            <td><input name="rok" type="number" /></td>
        </tr>
    </table>
    <textarea name="zprava"></textarea><br />

    <input type="submit" value="Odeslat" />
</form>

<script src="https://code.jquery.com/jquery-3.4.1.slim.min.js" integrity="sha384-J6qa4849blE2+poT4WnyKhv5vZF5SrPo0iEjwBvKU7imGFAV0wwj1yYfoRSJoZ+n" crossorigin="anonymous"></script>
<script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.0/dist/umd/popper.min.js" integrity="sha384-Q6E9RHvbIyZFJoft+2mJbHaEWldlvI9IOYy5n3zV9zzTtmI3UksdQRVvoxMfooAo" crossorigin="anonymous"></script>
<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/js/bootstrap.min.js" integrity="sha384-wfSDF2E50Y2D1uUdj0O3uMBJnjuUD4Ih7YwaYd1iqfktj0Uod8GCExl3Og8ifwB6" crossorigin="anonymous"></script>
</body>
</html>