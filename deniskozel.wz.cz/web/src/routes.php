<?php

use \Psr\Http\Message\ServerRequestInterface as Request;
use \Psr\Http\Message\ResponseInterface as Response;

$app->get('/', function (Request $request, Response $response, $args){
    return $this->view->render($response, 'index.latte');
})->setName('index');

$app->get('/barvy', function (Request $request, Response $response, $args){
    return $this->view->render($response, 'barvy.latte');
})->setName('barvy');

$app->get('/nadpis1', function (Request $request, Response $response, $args){
    return $this->view->render($response, 'nadpis1.latte');
})->setName('nadpis1');

$app->get('/nadpis2', function (Request $request, Response $response, $args){
    return $this->view->render($response, 'nadpis2.latte');
})->setName('nadpis2');

$app->get('/nadpis3', function (Request $request, Response $response, $args){
    return $this->view->render($response, 'nadpis3.latte');
})->setName('nadpis3');

$app->get('/test', function (Request $request, Response $response, $args){
    return $this->view->render($response, 'test.latte');
})->setName('test');

$app->post('/test-vyhodnoceni', function (Request $request, Response $response, $args){
    $data = $request->getParsedBody();
    $body = 0;

    for ($i = 1; $i<=10; $i++){
        if ($data[$i] === 'a') {
            $body = $body + 1;
        }
    }

    $tplVars['body'] = $body;
    return $this->view->render($response, 'test-vyhodnoceni.latte', $tplVars);
})->setName('test-vyhodnoceni');

$app->get('/piskvorky', function (Request $request, Response $response, $args){
    return $this->view->render($response, 'piskvorky.latte');
})->setName('piskvorky');

$app->get('/had', function (Request $request, Response $response, $args){
    return $this->view->render($response, 'had.latte');
})->setName('had');