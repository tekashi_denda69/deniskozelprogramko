package one.hanka;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;

public class HlavniOkno  extends JFrame {
    //TODO: Dopiš globální proměnné. POZOR! Jen ty co opravdu potřebuješ globálně
    private Container contentPane;
    private JButton btnVypocti;
    private JTextField fldPocetAut;
    private JTextField fldPocetMotorek;
    private JTextField fldSpotrebaAut;
    private JTextField fldSpotrebaMotorek;
    private JLabel lblPocetVozidel;
    private JLabel lblPocetKol;
    private JLabel lblPalivo;
    private JLabel lblPneu;
    private JLabel chyba;

    public void initComponents() {

        //Chtěl jsem přidat pozadí jako obrázek, ale nevyšlo mi to.//
        //Povedlo se mi obrázek dostat na plochu, ale nepovedlo se mi komponenty dostat na obrázek"


        /* začátek přednastavených komponent */
        JLabel labEvidence = new JLabel();
        JLabel labAuta = new JLabel();
        JLabel labMotorky = new JLabel();
//        JLabel labPalivo = new JLabel();

        setDefaultCloseOperation(WindowConstants.DISPOSE_ON_CLOSE);
        setTitle("Sberatel aut");
        contentPane = getContentPane();
        setSize(600, 450);
        setLocationRelativeTo(null);
        setVisible(true);
        getContentPane().setBackground(Color.orange);
        // nastavení komponent
        {
            contentPane.add(labEvidence);
            labEvidence.setBounds(150, 20, 370, 20);
            labEvidence.setText("Evidence aut a motorek");
            labEvidence.setFont(new Font("Dialog", Font.BOLD, 22));

            contentPane.add(labAuta);
            labAuta.setBounds(200, 60, 100, 30);
            labAuta.setText("Auta");
            labAuta.setFont(new Font("Segoe UI", Font.PLAIN, 18));

            contentPane.add(labMotorky);
            labMotorky.setBounds(465, 60, 100, 30);
            labMotorky.setText("Motorky");
            labMotorky.setFont(new Font("Segoe UI", Font.PLAIN, 18));


            JLabel labPocetAut = new JLabel();
            labPocetAut.setText("Počet aut");

            JLabel labSpotrebaAut = new JLabel();
            labSpotrebaAut.setText("Průmerná spotřeba");
            labSpotrebaAut.setBounds(20, 120, 150, 20);

            btnVypocti = new JButton("Spočítat");
            contentPane.add(btnVypocti);
            btnVypocti.setBounds(180, 180, 200, btnVypocti.getPreferredSize().height);
        }
        // vypočet preferované velikosti
        {
            // compute preferred size
            Dimension preferredSize = new Dimension();
            for(int i = 0; i < contentPane.getComponentCount(); i++) {
                Rectangle bounds = contentPane.getComponent(i).getBounds();
                preferredSize.width = Math.max(bounds.x + bounds.width, preferredSize.width);
                preferredSize.height = Math.max(bounds.y + bounds.height, preferredSize.height);
            }
            Insets insets = contentPane.getInsets();
            preferredSize.width += insets.right;
            preferredSize.height += insets.bottom;
            contentPane.setMinimumSize(preferredSize);
            contentPane.setPreferredSize(preferredSize);
        }
        /* konec přednastavených komponent */

        //TODO: Dopiš nastavení komponent dle sceenshotu aplikace
        draw1();
        draw2();
        draw3();

        //TODO: Dopiš metodu po kliknutí dle sceenshotu aplikace. Celá stáj najede za den průměrně 98 km denně (každé vozidlo). Pneu měníme každý rok.
        btnVypocti.addActionListener(e -> spocitej());

        this.getRootPane().setDefaultButton(btnVypocti);
    }

    private void draw1() {
        JLabel lblPocetAut = new JLabel();
        JLabel lblPocetMotorek = new JLabel();
        JLabel lblSpotrebaAut = new JLabel();
        JLabel lblSpotrebaMotorek = new JLabel();
        fldPocetAut = new JTextField();
        fldPocetMotorek = new JTextField();
        fldSpotrebaAut = new JTextField();
        fldSpotrebaMotorek = new JTextField();
        contentPane.add(lblPocetAut);
        contentPane.add(lblPocetMotorek);
        contentPane.add(lblSpotrebaAut);
        contentPane.add(lblSpotrebaMotorek);
        contentPane.add(fldPocetAut);
        contentPane.add(fldPocetMotorek);
        contentPane.add(fldSpotrebaAut);
        contentPane.add(fldSpotrebaMotorek);

        lblPocetAut.setText("Počet");
        lblPocetMotorek.setText("Počet");
        lblSpotrebaAut.setText("Průměrná spotřeba");
        lblSpotrebaMotorek.setText("Průměrná spotřeba");

        lblPocetAut.setBounds(20, 100, 150, 20);
        lblPocetMotorek.setBounds(300, 100, 150, 20);
        lblSpotrebaAut.setBounds(20, 130, 150, 20);
        lblSpotrebaMotorek.setBounds(300, 130, 150, 20);
        fldPocetAut.setBounds(170, 100, 100, 20);
        fldPocetMotorek.setBounds(450, 100, 100, 20);
        fldSpotrebaAut.setBounds(170, 130, 100, 20);
        fldSpotrebaMotorek.setBounds(450, 130, 100, 20);
    }

    private void draw2() {

        JLabel lblVelikostStaje = new JLabel();
        lblPocetVozidel = new JLabel();
        lblPocetKol = new JLabel();

        contentPane.add(lblVelikostStaje);
        contentPane.add(lblPocetVozidel);
        contentPane.add(lblPocetKol);

        lblVelikostStaje.setText("Velikost stáje na konci roku");
        lblVelikostStaje.setFont(new Font("Segoe UI", Font.PLAIN, 18));
        lblPocetVozidel.setText("");
        lblPocetKol.setText("");


        lblVelikostStaje.setBounds(175, 240, 300, 30);
        lblPocetVozidel.setBounds(200, 280, 150, 20);
        lblPocetKol.setBounds(300, 280, 150, 20);
    }

    private void draw3() {

        JLabel lblCelkovaSpotreba = new JLabel();
        lblPalivo = new JLabel();
        lblPneu = new JLabel();

        contentPane.add(lblCelkovaSpotreba);
        contentPane.add(lblPalivo);
        contentPane.add(lblPneu);

        lblCelkovaSpotreba.setText("Celková spotřeba");
        lblCelkovaSpotreba.setFont(new Font("Segoe UI", Font.PLAIN, 18));
        lblPalivo.setText("");
        lblPneu.setText("");


        lblCelkovaSpotreba.setBounds(200, 330, 300, 30);
        lblPalivo.setBounds(150, 370, 150, 20);
        lblPneu.setBounds(300, 370, 150, 20);

        chyba = new JLabel();
        contentPane.add(chyba);
        chyba.setBounds(450, 20, 150, 30);
        chyba.setForeground(new Color(255, 0, 0));
        chyba.setFont(new Font("Segoe UI", Font.BOLD, 18));
    }

    private void spocitej() {
        try {
            chyba.setText(" ");

            int pocetVozidel = Integer.parseInt(fldPocetAut.getText()) + Integer.parseInt(fldPocetMotorek.getText());
            lblPocetVozidel.setText(pocetVozidel + " vozidel");
            int pocetKol = Integer.parseInt(fldPocetAut.getText()) * 4 + Integer.parseInt(fldPocetMotorek.getText()) * 2;
            lblPocetKol.setText(pocetKol + " kol");

            int spotrebaAut = Integer.parseInt(fldPocetAut.getText()) * Integer.parseInt(fldSpotrebaAut.getText()) * 98 * 365;
            int spotrebaMotorek = Integer.parseInt(fldPocetMotorek.getText()) * Integer.parseInt(fldSpotrebaMotorek.getText()) * 98 * 365;
            int spotreba = spotrebaAut + spotrebaMotorek;
            lblPalivo.setText(spotrebaAut + " litrů paliva");

            int pneu = pocetKol * 2;
            lblPneu.setText(pneu + " pneumatik");

            fldPocetAut.setText("");
            fldPocetMotorek.setText("");
            fldSpotrebaAut.setText("");
            fldSpotrebaMotorek.setText("");
            fldPocetAut.grabFocus();
        }
        catch (Exception ex) {
            chyba.setText("Chyba");

            fldPocetAut.setText("");
            fldPocetMotorek.setText("");
            fldSpotrebaAut.setText("");
            fldSpotrebaMotorek.setText("");
            lblPocetVozidel.setText(" ");
            lblPocetKol.setText(" ");
            lblPalivo.setText(" ");
            lblPneu.setText(" ");
            fldPocetAut.grabFocus();
        }
    }
}
