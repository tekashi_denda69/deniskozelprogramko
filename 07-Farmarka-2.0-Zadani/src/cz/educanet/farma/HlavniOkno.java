package cz.educanet.farma;

import java.awt.*;
import java.awt.event.ActionEvent;
import javax.swing.*;

public class HlavniOkno extends JFrame {

    // JFormDesigner - Variables declaration - DO NOT MODIFY  //GEN-BEGIN:variables
    // Generated using JFormDesigner Evaluation license - unknown
    // JFormDesigner - End of variables declaration  //GEN-END:variables

    Integer Pocetsamcu1;
    Integer Pocetsamcu2;
    Integer Pocetsamic1;
    Integer Pocetsamic2;
    JTextField txtPocetsamcu1 = new JTextField();
    JTextField txtPocetsamcu2 = new JTextField();
    JTextField txtPocetsamic1 = new JTextField();
    JTextField txtPocetsamic2 = new JTextField();

    public void initComponents() {
        // JFormDesigner - Component initialization - DO NOT MODIFY  //GEN-BEGIN:initComponents
        // Generated using JFormDesigner Evaluation license - unknown

        //======== this ========
        setDefaultCloseOperation(WindowConstants.DISPOSE_ON_CLOSE);
        setTitle("Farm\u00e1\u0159ka 2.0");
        Container contentPane = getContentPane();
        contentPane.setLayout(null);

        {
            // compute preferred size
            Dimension preferredSize = new Dimension();
            for(int i = 0; i < contentPane.getComponentCount(); i++) {
                Rectangle bounds = contentPane.getComponent(i).getBounds();
                preferredSize.width = Math.max(bounds.x + bounds.width, preferredSize.width);
                preferredSize.height = Math.max(bounds.y + bounds.height, preferredSize.height);
            }
            Insets insets = contentPane.getInsets();
            preferredSize.width += insets.right;
            preferredSize.height += insets.bottom;
            contentPane.setMinimumSize(preferredSize);
            contentPane.setPreferredSize(preferredSize);
        }
        setSize(605, 450);
        setLocationRelativeTo(null);
        // JFormDesigner - End of component initialization  //GEN-END:initComponents
        JTextField txtPocetsamcu1 = new JTextField();
        JTextField txtPocetsamcu2 = new JTextField();
        JTextField txtPocetsamic1 = new JTextField();
        JTextField txtPocetsamic2 = new JTextField();

        JButton btnTlacitko = new JButton();
        btnTlacitko.setBounds(30, 180, 540, 30);
        btnTlacitko.setText("Spočítat");

        JLabel labNadpis = new JLabel();
        labNadpis.setText("Evidence králíků a hus");
        labNadpis.setBounds(10, 5, 540, 30);

        JLabel labKralici = new JLabel();
        labKralici.setText("Králící");
        labKralici.setFont(new Font("TimesRoman", Font.BOLD, 32));
        labKralici.setBounds(50, 50, 540, 30);

        JLabel labHusy = new JLabel();
        labHusy.setText("Husy");
        labHusy.setFont(new Font("TimesRoman", Font.BOLD, 32));
        labHusy.setBounds(350, 50, 540, 30);
        JLabel labPocetsamcu1 = new JLabel();
        labPocetsamcu1.setText("Počet samců");
        labPocetsamcu1.setBounds(50, 150, 540,30);
        txtPocetsamcu1.setBounds(125, 158, 80, 20);

        JLabel labPocetsamcu2 = new JLabel();
        labPocetsamcu2.setText("Počet samců");
        labPocetsamcu2.setBounds(350, 150, 540,30);
        txtPocetsamcu2.setBounds(425, 158, 80, 20);

        JLabel labPocetsamic1 = new JLabel();
        labPocetsamic1.setText("Počet samic");
        labPocetsamic1.setBounds(50, 125, 540,30);
        txtPocetsamic1.setBounds(125, 134, 80, 20);

        JLabel labPocetsamic2 = new JLabel();
        labPocetsamic2.setText("Počet samic");
        labPocetsamic2.setBounds(350, 125, 540,30);
        txtPocetsamic2.setBounds(425, 134, 80, 20);


        JLabel labVelikost = new JLabel();
        labVelikost.setText("Velikost chovu před zimou:");
        labVelikost.setBounds(50, 215, 540,30);
        labVelikost.setFont(new Font("TimesRoman", Font.BOLD, 16));

        JLabel labPotreba = new JLabel();

        contentPane.add(labNadpis);
        contentPane.add(labKralici);
        contentPane.add(labHusy);
        contentPane.add(labPocetsamcu1);
        contentPane.add(labPocetsamcu2);
        contentPane.add(labPocetsamic1);
        contentPane.add(labPocetsamic2);
        contentPane.add(txtPocetsamcu1);
        contentPane.add(txtPocetsamcu2);
        contentPane.add(txtPocetsamic1);
        contentPane.add(txtPocetsamic2);
        contentPane.add(btnTlacitko);
        contentPane.add(labVelikost);

        btnTlacitko.addActionListener(e -> poKliknuti(e));

    }

    private void poKliknuti(ActionEvent e) {

    }
}
